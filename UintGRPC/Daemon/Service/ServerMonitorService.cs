﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using Uint.Daemon.RPC;
using Status = Uint.Daemon.RPC.Status;

namespace Uint.Daemon.Service
{
    public class ServerMonitorService : ServerMonitor.ServerMonitorBase
    {
        public override Task<Response> getStatus(Request request, ServerCallContext context)
        {
            Console.WriteLine(request.Host);
            var status = Status.Successful;
            
            return Task.FromResult(new Response
            {
                Status = status,
                Message = "Server is up and running"
            });
        }
    }
}