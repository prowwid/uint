﻿using System;
using System.Runtime.Loader;
using System.Threading;

namespace Uint.Daemon
{
    class Daemon
    {
        private static App app;
        private static readonly AutoResetEvent resetEvent = new AutoResetEvent(false);

        static void Main(string[] args)
        {
            app = new App();
            app.CreateAndStartServer();

            Console.WriteLine("Server is started and listening 5000 port");
            
            resetEvent.WaitOne();
        }

        private static void SetUpSignalHandlers()
        {
            //register sigterm event handler.
            AssemblyLoadContext.Default.Unloading += SigTermEventHandler;
            //register sigint event handler
            Console.CancelKeyPress += CancelHandler;
        }

        private static void SigTermEventHandler(AssemblyLoadContext obj)
        {
            Console.WriteLine("Unloading...");

            app.FreeResources();
        }

        private static void CancelHandler(object sender, ConsoleCancelEventArgs e)
        {
            Console.WriteLine("Exiting...");

            app.FreeResources();
        }
    }
}