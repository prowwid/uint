﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using Uint.Daemon.RPC;
using Uint.Daemon.Service;

namespace Uint.Daemon
{
    public class App
    {
        private Grpc.Core.Server server;

        public void CreateAndStartServer()
        {
            var host = "localhost";
            var port = 5000;
            server = new Server
            {
                Services =
                {
                    ServerMonitor.BindService(new ServerMonitorService()),
                },
                Ports = {new ServerPort(host, port, ServerCredentials.Insecure)}
            };
            server.Start();
        }

        public void FreeResources()
        {
            var tasks = new List<Task>();

            tasks.Add(server.ShutdownAsync());

            Task.WaitAll(tasks.ToArray());
        }
    }
}