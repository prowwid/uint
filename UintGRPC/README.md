# Uint gRPC Server

Sample [gRPC](https://grpc.io) server app.

# Overview
The app entry point is Daemon.cs file. 

The app listen to 5000 port? handling calls to a ServerMonitor service defined at Protocols\ServerMonitor.proto

##Run your app

```
dotnet run
```

## Updating gRPC services

once proto file is updated you can generate new code using this stub for UNIX systems:
```
~/.nuget/packages/grpc.tools/{installed version}/tools/linux_x64/protoc -I{path to project}/Daemon/Protocols --csharp_out $(pwd)/grpc-build --grpc_out $(pwd)/grpc-build $(pwd)/RPC/Protocols/{proto file name}.proto --plugin=protoc-gen-grpc=${HOME}/.nuget/packages/grpc.tools/{installed version}/tools/linux_x64/grpc_csharp_plugin
```

Example:
```
~/.nuget/packages/grpc.tools/1.17.1/tools/linux_x64/protoc -I$(pwd)/Daemon/Protocols --csharp_out $(pwd)/grpc-build --grpc_out $(pwd)/job-queue/grpc-build $(pwd)/RPC/Daemon/ServerMonitor.proto --plugin=protoc-gen-grpc=${HOME}/.nuget/packages/grpc.tools/1.17.1/tools/linux_x64/grpc_csharp_plugi
```

for Windows command stub looks like this:
```
%userprofile%\.nuget\packages\grpc.tools\{installed version}\tools\windows_x64\protoc.exe --proto_path=%cd%\Daemon\Protocols --csharp_out=%cd%\grpc-build --grpc_out=%cd%\grpc-build %cd%\Daemon\Protocols\ServerMonitor.proto --plugin=protoc-gen-grpc=%userprofile%\.nuget\packages\grpc.tools\{installed version}\tools\windows_x64\grpc_csharp_plugin.exe
```

Example:
```
%userprofile%\.nuget\packages\grpc.tools\2.23.0\tools\windows_x64\protoc.exe --proto_path=%cd%\Daemon\Protocols --csharp_out=%cd%\grpc-build --grpc_out=%cd%\grpc-build %cd%\Daemon\Protocols\ServerMonitor.proto --plugin=protoc-gen-grpc=%userprofile%\.nuget\packages\grpc.tools\2.23.0\tools\windows_x64\grpc_csharp_plugin.exe
```
