import {BrowserModule} from '@angular/platform-browser'
import {NgModule} from '@angular/core'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap'
import {HighlightModule} from 'ngx-highlightjs'
import json from 'highlight.js/lib/languages/json'
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome'

import {AppRoutingModule} from './app-routing.module'
import {AppComponent} from './ui/layout/app/app.component'
import {CallbackComponent} from './ui/pages/callback/callback.component'
import {HomeComponent} from './ui/pages/home/root/home.component'
import {ProfileComponent} from './ui/pages/profile/profile.component'
import {NavBarComponent} from './ui/layout/nav-bar/nav-bar.component'
import {FooterComponent} from './ui/layout/footer/footer.component'
import {HeroComponent} from './ui/pages/home/components/hero/hero.component'
import {HomeContentComponent} from './ui/pages/home/components/home-content/home-content.component'
import {LoadingComponent} from './ui/components/loading/loading.component'
import {SalesComponent} from './ui/pages/sales/sales.component'
import {HttpClientModule} from '@angular/common/http'
import {GrpcComponent} from './ui/pages/grpc/grpc.component'
import {AccessComponent} from './ui/pages/access/access.component'

export function hljsLanguages() {
  return [{name: 'json', func: json}]
}

@NgModule({
  declarations: [
    AppComponent,
    CallbackComponent,
    HomeComponent,
    ProfileComponent,
    NavBarComponent,
    FooterComponent,
    HeroComponent,
    HomeContentComponent,
    LoadingComponent,
    SalesComponent,
    GrpcComponent,
    AccessComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    HighlightModule.forRoot({
      languages: hljsLanguages
    }),
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
