import {Injectable} from '@angular/core'
import {HttpClient, HttpResponse} from '@angular/common/http'
import {Observable} from 'rxjs'
import {SaleEntity} from '../../domain/models/sale/sale.entity'
import {map} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class Api {

  constructor(private http: HttpClient) {
  }

  ping$(): Observable<any> {
    return this.http.get('/api')
  }

  getServerStatus(): Observable<any> {
    return this.http.get('/api/grpc/status')
  }

  loadSales(): Observable<any> {
    return this.http.get('/api/sales').pipe(
      map((data: ApiResponse) => {
          return data.result.map((item: any) => {
            return new SaleEntity().deserialize(item)
          })
        }
      ),
    )
  }
}

interface ApiResponse {
  result: object[]
}
