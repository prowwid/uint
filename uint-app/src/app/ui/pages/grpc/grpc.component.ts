import {Component, OnInit} from '@angular/core'
import {Api} from 'src/app/core/api/api.service'

@Component({
  selector: 'app-grpc',
  templateUrl: './grpc.component.html',
  styleUrls: ['./grpc.component.css']
})
export class GrpcComponent implements OnInit {
  responseJson: string

  constructor(private api: Api) {
  }

  ngOnInit() {
  }

  getServerStatus() {
    this.api.getServerStatus().subscribe(
      res => this.responseJson = JSON.stringify(res, null, 2).trim()
    )
  }

}
