import {async, ComponentFixture, TestBed} from '@angular/core/testing'

import {GrpcComponent} from './grpc.component'

describe('ExternalApiComponent', () => {
  let component: GrpcComponent
  let fixture: ComponentFixture<GrpcComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GrpcComponent]
    })
           .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(GrpcComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
