import {Component, OnInit} from '@angular/core'
import {Api} from 'src/app/core/api/api.service'
import {SaleEntity} from '../../../domain/models/sale'

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.css']
})
export class SalesComponent implements OnInit {
  hasSales: boolean
  sales: Array<SaleEntity>

  constructor(private api: Api) {
  }

  ngOnInit() {
  }

  pingApi() {
    this.api.loadSales().subscribe(
      res => {
        this.sales = res

        this.hasSales = Boolean(this.sales.length)
      }
    )
  }

}
