import {Component, OnInit} from '@angular/core'
import {Api} from 'src/app/core/api/api.service'

@Component({
  selector: 'app-access',
  templateUrl: './access.component.html',
  styleUrls: ['./access.component.css']
})
export class AccessComponent implements OnInit {
  responseJson: string

  constructor(private api: Api) {
  }

  ngOnInit() {
  }

  pingApi() {
    this.api.ping$().subscribe(
      res => this.responseJson = JSON.stringify(res, null, 2).trim()
    )
  }

}
