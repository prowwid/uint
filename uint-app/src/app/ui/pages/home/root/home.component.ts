import {Component, OnInit} from '@angular/core'
import {Authenticator} from 'src/app/core/auth/authenticator.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(public auth: Authenticator) {
  }

  ngOnInit() {
  }

}
