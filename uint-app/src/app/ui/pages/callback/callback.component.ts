import {Component, OnInit} from '@angular/core'
import {Authenticator} from 'src/app/core/auth/authenticator.service'

@Component({
  selector: 'app-callback',
  templateUrl: './callback.component.html',
  styleUrls: ['./callback.component.css']
})
export class CallbackComponent implements OnInit {

  constructor(private auth: Authenticator) {
  }

  ngOnInit() {
    this.auth.handleAuthCallback()
  }

}
