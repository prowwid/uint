import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'
import {HomeComponent} from './ui/pages/home/root/home.component'
import {CallbackComponent} from './ui/pages/callback/callback.component'
import {AuthGuard} from './core/auth/auth.guard'
import {ProfileComponent} from './ui/pages/profile/profile.component'
import {SalesComponent} from './ui/pages/sales/sales.component'
import {HTTP_INTERCEPTORS} from '@angular/common/http'
import {InterceptorService} from './core/auth/interceptor.service'
import {AccessComponent} from './ui/pages/access/access.component'
import {GrpcComponent} from './ui/pages/grpc/grpc.component'

const routes: Routes = [
  {
    path: 'callback',
    component: CallbackComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'sales',
    component: SalesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'access',
    component: AccessComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'grpc',
    component: GrpcComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ]
})
export class AppRoutingModule {
}
