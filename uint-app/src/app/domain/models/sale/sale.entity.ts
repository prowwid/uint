import {Deserializable} from '../../contracts/deserializable.contract'
import {Item} from './item'
import {Customer} from './customer'

export class SaleEntity implements Deserializable {
  id: number
  saleDate: string
  storeLocation: string
  purchaseMethod: string
  couponUsed: boolean
  customer?: Customer
  items?: Item[]

  deserialize(input: any) {
    Object.assign(this, input)

    this.customer = new Customer().deserialize(input.customer)
    this.items = new Array<Item>()
    input.items.forEach(item => {
      this.items.push(new Item().deserialize(item))
    })

    return this
  }
}
