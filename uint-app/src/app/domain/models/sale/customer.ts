export class Customer {
  gender: string
  email: string
  age: number
  satisfaction: number

  deserialize(input: any) {
    Object.assign(this, input)

    return this
  }
}
