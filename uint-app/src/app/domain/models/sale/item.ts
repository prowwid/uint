import {Deserializable} from '../../contracts/deserializable.contract'
import {Customer} from './customer'

export class Item implements Deserializable {
  name: string
  price: number
  quantity: number
  tags: string[]

  deserialize(input: any) {
    Object.assign(this, input)

    return this
  }
}
