# Uint App

This sample app demonstrates Aut0 login system and how it can be used together with Express API and [gRPC](https://grpc.io).

# Overview

The app follows layered architecture. The main layers are:
```
ui        Presentation layer that handle all of the user interaction logic
domain    Business logic layer that keep all of the models, domain services and etc.
core      Core layer, that keep all of the base classes that upper layers need to operate. 
```
Root layers also split into sub-layers or categories.

## UI layer
```
components   common and base UI components
directives   Angular directives
layout       app layout components
pages        app pages
pipes        Angular pipes
services     UI services
```

## Domain layer
```
contracts   domain layer interfaces
models      domain models
strategies  business logic strategies
services    business logic services
```

## Configuration

The sample needs to be configured with your Auth0 domain and client ID in order to work. In the root of the sample, copy `auth-config.json.example` and rename it to `auth-config.json`. Open the file and replace the values with those from your Auth0 tenant:

```json
{
  "domain": "<YOUR AUTH0 DOMAIN>",
  "clientId": "<YOUR AUTH0 CLIENT ID>",
  "audience": "<YOUR AUTH0 API AUDIENCE IDENTIFIER>"
}
```
If you don't have a tenant go to [Auth0](https://auth0.com/signup) and click Sign Up.

## Development server

Run `npm run dev` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Run Using Docker

Run `npm run docker` to build the project.
