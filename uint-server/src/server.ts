import errorHandler from 'errorhandler'

import app from './app'

if (process.env.SERVER_TYPE && process.env.SERVER_TYPE == 'prod') {
    /**
     * Error Handler. Provides full stack for dev installations
     */
    app.use(errorHandler())
}


/**
 * Start Express server.
 */
const server = app.listen(app.get('port'), () => {
    console.log(
        '  App is running at http://localhost:%d in %s mode',
        app.get('port'),
        app.get('env')
    )
    console.log('  Press CTRL-C to stop\n')
})

export default server
