import {Response, Request, NextFunction} from 'express'
import {Sale} from '../../../domain/models/sale'

/**
 * Lists first 5 sales from "sales" collection
 */
export async function ListSales(req: Request, res: Response, next: NextFunction) {
    try {
        Sale.find()
            .sort({'saleDate': -1})
            .limit(5)
            .exec((err, result) => {
                res.send({
                    result
                })
            })
    } catch (e) {
        next(e)
    }
}