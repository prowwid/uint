import {Response, Request, NextFunction} from 'express'
import grpc from 'grpc'
import {Request as RPCRequest, Response as RPCResponse} from './../../../grpc/client/ServerMonitor_pb'
import {ServerMonitorClient} from './../../../grpc/client/ServerMonitor_grpc_pb'

/**
 * Check status of .NET Core server using gRPC
 */
export async function ServerStatus(req: Request, res: Response, next: NextFunction) {
    let client = new ServerMonitorClient('localhost:5000', grpc.credentials.createInsecure())

    let request = new RPCRequest()
    request.setHost('localhost')

    try {
        client.getStatus(request, function (err: any, response: RPCResponse) {
            res.send({
                status: response.getStatus() == 1 ? 'Success' : 'Failure',
                message: response.getMessage(),
            })
        })
    } catch (e) {
        next(e)
    }
}