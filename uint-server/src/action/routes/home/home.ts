import {Response, Request, NextFunction} from 'express'

/**
 * Represents a root route that can be used to validates access to the API.
 */
export function Home(req: Request, res: Response, next: NextFunction) {
    res.send({
        message: 'You are authorized to use API!'
    })
}
