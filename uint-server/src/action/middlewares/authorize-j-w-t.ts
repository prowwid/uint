import jwt from 'express-jwt'
import jwksRsa from 'jwks-rsa'
import {AUTH_AUDIENCE, AUTH_DOMAIN} from '../../env/secrets'

/**
 * Authorize JWT for Auth0 account specified in config.
 */
export const AuthorizeJWT = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: `https://${AUTH_DOMAIN}/.well-known/jwks.json`
    }),

    audience: AUTH_AUDIENCE,
    issuer: `https://${AUTH_DOMAIN}/`,
    algorithm: ['RS256']
})