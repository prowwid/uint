import express from 'express'
import compression from 'compression'  // compresses requests
import bodyParser from 'body-parser'
import lusca from 'lusca'
import 'reflect-metadata'
import mongoose from 'mongoose'
import {MONGODB_URI} from './env/secrets'

mongoose.connect(MONGODB_URI, {useNewUrlParser: true})

// Middlewares
import {AuthorizeJWT} from './action/middlewares/authorize-j-w-t'

// Actions (route handlers)
import {Home} from './action/routes/home/home'
import {ListSales} from './action/routes/sales/list-sales'
import {ServerStatus} from './action/routes/grpc/server-status'

// Create Express server
const app = express()

// Express configuration
app.set('port', process.env.PORT || 4000)

app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))
app.use(lusca.xssProtection(true))

/**
 * Primary app routes.
 */
app.get('/', AuthorizeJWT, Home)
app.get('/api', AuthorizeJWT, Home)
app.get('/api/sales', AuthorizeJWT, ListSales)
app.get('/api/grpc/status', AuthorizeJWT, ServerStatus)

export default app
