import mongoose from 'mongoose'

export type SaleDocument = mongoose.Document & {
    saleDate: string
    storeLocation: string
    purchaseMethod: string
    couponUsed: boolean
    customer?: Customer
    items?: Item[]
};

interface Customer {
    gender: string
    email: string
    age: number
    satisfaction: number
}

interface Item {
    name: string
    price: number
    quantity: number
}

const saleSchema = new mongoose.Schema({
    storeLocation: String,
    saleDate: {type: Date, default: Date.now},
    purchaseMethod: String,
    couponUsed: Boolean,
    customer: {
        gender: String,
        email: String,
        age: Number,
        satisfaction: Number
    },
    items: [{
        name: String,
        price: Number,
        quantity: Number
    }]
}, {timestamps: true})


export const Sale = mongoose.model<SaleDocument>('Sale', saleSchema, 'sales')
