// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('grpc');
// var ServerMonitor_pb = require('./ServerMonitor_pb.js');
import {Request, Response} from './ServerMonitor_pb';

function serialize_Uint_Daemon_RPC_Request(arg: any) {
  if (!(arg instanceof Request)) {
    throw new Error('Expected argument of type Uint.Daemon.RPC.Request');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_Uint_Daemon_RPC_Request(buffer_arg: any) {
  return Request.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_Uint_Daemon_RPC_Response(arg: any) {
  if (!(arg instanceof Response)) {
    throw new Error('Expected argument of type Uint.Daemon.RPC.Response');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_Uint_Daemon_RPC_Response(buffer_arg:any) {
  return Response.deserializeBinary(new Uint8Array(buffer_arg));
}


var ServerMonitorService = exports.ServerMonitorService = {
  getStatus: {
    path: '/Uint.Daemon.RPC.ServerMonitor/getStatus',
    requestStream: false,
    responseStream: false,
    requestType: Request,
    responseType: Response,
    requestSerialize: serialize_Uint_Daemon_RPC_Request,
    requestDeserialize: deserialize_Uint_Daemon_RPC_Request,
    responseSerialize: serialize_Uint_Daemon_RPC_Response,
    responseDeserialize: deserialize_Uint_Daemon_RPC_Response,
  },
};
export const ServerMonitorClient = grpc.makeGenericClientConstructor(ServerMonitorService);