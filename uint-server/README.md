# Uint Server

This sample app demonstrates Aut0 login authorisation using Express and basic usage of [gRPC](https://grpc.io).

# Overview

The app follows layered architecture. The main layers are:
```
action    Outer layer that takeand process incoming call. All the business logic is delegated to domain layer 
domain    Business logic layer that keep all of the models, domain services and etc.
core      Core layer, that keep all of the base classes that upper layers need to operate. 
```
Root layers also split into sub-layers or categories.

## Action layer
```
middlewares  middlewares for app routes
routes       application routes that handle incoming API calls
strategies   routing strategies
services     routing services
```

## Domain layer
```
contracts   domain layer interfaces
models      domain models
strategies  business logic strategies
services    business logic services
```


# Pre-requirements
To build and run this app locally you will need a few things:
- Install [Node.js](https://nodejs.org/en/)
- Install [MongoDB](https://docs.mongodb.com/manual/installation/) or register at [MongoDB Atlas](https://www.mongodb.com/cloud/atlas)

# Prepare environment

Install global gRPC dependencies required to generate definition from proto files

```
npm install -g grpc-tools
npm install -g ts-protoc-gen
```

## Configuration

The sample needs to be configured with your Auth0 domain and audience in order to work. In the root of the sample, copy `.env.example` and rename it to `.env`. Open the file and replace the values with those from your Auth0 tenant:

```
AUTH_DOMAIN=<domain name>
AUTH_AUDIENCE=<audience url>
```
If you don't have a tenant go to [Auth0](https://auth0.com/signup) and click Sign Up.

Also set MongoDB connection string to:
```
MONGODB_URI=mongodb://<user>:<password>@<connection_url>
``` 

# Updating gRPC services
Generate JS files
```
grpc_tools_node_protoc --js_out=import_style=commonjs,binary:%cd%\src\grpc\client --grpc_out=%cd%\src\grpc\client --plugin=protoc-gen-grpc=%userprofile%\AppData\Roaming\npm\node_modules\grpc-tools\bin\grpc_node_plugin.exe --proto_path %cd%\src\grpc\protocols %cd%\src\grpc\protocols\ServerMonitor.proto
```
Generate d.ts

```
grpc_tools_node_protoc --plugin=protoc-gen-ts=%userprofile%\AppData\Roaming\npm\node_modules\ts-protoc-gen\bin\protoc-gen-ts.cmd --ts_out=%cd%\src\grpc\client --proto_path %cd%\src\grpc\protocols %cd%\src\grpc\protocols\ServerMonitor.proto
```