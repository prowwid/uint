import request from "supertest";
import app from "../src/app";

describe("home action", () => {
    it("should return 401 Unauthorized", (done) => {
        request(app).get("/")
            .expect(401, done)
    })
})
